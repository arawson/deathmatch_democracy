#Deathmatch Democracy: Alpha 0.2#

##Description##
A simple deathmatch mode for Garry's Mod.

##Installation:##
Clone the repository in your garrysmod/gamemodes directory and you should be good to go.

Currently, there is not a workshop release. I would prefer if you would wait until then. If you can't wait, the above instructions should work.

##Design Items & Development Progress##
|# |Item |% Complete |Notes |
|--- |--- |--: |--- |
|1 |A match consists of 3 phases. |0% |
|1.1 |The pre-match phase consists of players voting on which weapons should be allowed in the following rounds.| %100 |
|1.1.2 |The maximum number of weapons in the rounds is configurable using the `dd_weapon_pool_size` variable. |%100 |
|1.1.3 |The game determines the weapon pool from the players votes with random chance used to break ties. |100% |
|1.2 |The round phase consists of repeating rounds. | 100% |
|1.2.1 |A round begins with players selecting which, of the winning group, weapons they want to use for the round. | 100% |
|1.1.2 |The maximum number of weapons a player can select is determined by the `dd_max_weapons` variable. |100% |
|1.2.2 |A round's main phase is the deathmatch. |100% |
|1.2.3 |The round returns to the selection phase after a set number of minutes. |100% |
|1.2.4 |The round ends after a set amount of rounds. |100% |
|1.3 | After the end of the round phase, the game returns to the voting phase. |100% |
|1.4 |If there are no more rounds in the game, then proceed to the next map on rotation. |0% |
|1.5 |Between rounds, the players are shown the current scoreboard. |100% |
|2 |Weapon initial ammunition should be configurable. |0% |
|2.1 |Infinit weapon ammo should be allowed through the config variable `dd_ammo_infinite`.| 0% |
|2.2 |Weapon ammo should be read from the config variable `dd_ammo_override`, this overrides the later options.|
|2.3 |Weapon ammo should be read from the config file. |0% |

##License:##
I release my source code into the public domain. My modified icons, however, are released [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)
##Credits:##
The original icons are [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) by [Freepik](http://www.freepik.com "Freepik") from [Flaticon](https://flaticon.com "Flaticon").

The [Deja Vu Fonts](http://dejavu-fonts.org/wiki/index.php?title=Main_Page) are under a [free license](http://dejavu-fonts.org/wiki/License).

if not util then return end

local math = math
local string = string
local table = table
local pairs = pairs

--simple time from ttt
function util.SimpleTime(seconds, fmt)
	if not seconds then seconds = 0 end
	
	local ms = (seconds - math.floor(seconds)) * 100
	seconds = math.floor(seconds)
	local s = seconds % 60
	seconds = (seconds - s) / 60
	local m = seconds % 60
	
	return string.format(fmt, m, s, ms)
end


--serverside extensions to player table
local plymeta = FindMetaTable("Player")
if not plymeta then Error("FAILED TO FIND PLAYER TABLE") return end

local oldSpectate = plymeta.Spectate
function plymeta:Spectate(type)
	oldSpectate(self, type)
	self.is_spectator = true
end

local oldUnSpectate = plymeta.UnSpectate
function plymeta:UnSpectate()
	oldUnSpectate(self)
	self.is_spectator = false
end

function plymeta:CanSpawn()
	return self:IsLoadoutReady() and not self.is_dead
end

function plymeta:SetDead(value)
	self.is_dead = value
end

function plymeta:IsSpectator()
	return self.is_spectator
end

function plymeta:HasSpawnedThisRound()
	return self.has_spawned
end

function plymeta:SpawnForRound()
	self:StripAll()
	
	if not self:IsLoadoutReady() then
		return true
	end
	
	self:GiveSelectedWeapons()
	
	self.has_spawned = true
	self:SetDead(false)
	
	self:Spawn()
	
	return true
end

function plymeta:SpawnForOther()
	self:StripAll()
	self:Spawn()
	self:Spectate(OBS_MODE_ROAMING)
	self.has_spawned = false
	
	return true;
end

function plymeta:StripAll()
	self:StripAmmo()
	self:StripWeapons()
end

function plymeta:GiveSelectedWeapons()
	local weps = self.weapon_loadout_buffered
	for w,_ in pairs(weps) do
		weapon = weapons.Get(w)
		--TODO make it so that weapons can have server-configurable ammo amounts
		if weapon.PrimaryAmmo then
			self:GiveAmmo(100, weapon.Primary.Ammo)
		end
		if weapon.Secondary.Ammo then
			self:GiveAmmo(100, weapon.Secondary.Ammo)
		end
		self:Give(w)
	end
end

function plymeta:CountLoadout()
	local wepcount = 0
	for wep, _ in pairs(self.weapon_loadout) do wepcount = wepcount + 1 end
	return wepcount
end

function plymeta:SetLoadoutReady(ready)
	self.loadout_ready = ready
end

function plymeta:InitWeaponLoadout()
	self.weapon_loadout = {}
	self.weapon_loadout_buffered = {}
	self:SetLoadoutReady(false)
end

function plymeta:IsLoadoutReady()
	return self.loadout_ready
end

function plymeta:CalculateLoadoutReady()
	local wepcount = self:CountLoadout()
	if wepcount >= MATCH.max_weapons then
		self:SetLoadoutReady(true)
	else
		self:SetLoadoutReady(false)
	end
end

--TODO delayed respawn
--function plymeta:SetSpawnTime

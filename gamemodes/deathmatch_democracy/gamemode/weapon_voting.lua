
function WEAPONS.GetPlayerVoteCount(ply)
	local wlist = WEAPONS.GetVoteListForPlayer(ply)
	local plvotes = 0
	
	for wepclass, voted in pairs(wlist) do
		if voted then
			plvotes = plvotes + 1
		end
	end
	return plvotes
end

function WEAPONS.PlayerBelowVoteLimit(ply)
	return WEAPONS.GetPlayerVoteCount(ply) < GetConVarNumber("dd_max_weapons")
end

function WEAPONS.PlayerAboveVoteLimit(ply)
	return WEAPONS.GetPlayerVoteCount(ply) > GetConVarNumber("dd_max_weapons")
end

--done at end of voting phase!
function WEAPONS.Recount()
	for k, w in pairs(WEAPONS.votes) do
		WEAPONS.votes[k] = 0
	end
	
	for ply, wlist in pairs(WEAPONS.player_votes) do
		--if player has too many votes, wipe them all!
		if WEAPONS.PlayerAboveVoteLimit(ply) then
			wlist = {}
		end
		
		for wep, voted in pairs(wlist) do
			if voted then
				WEAPONS.votes[wep] = WEAPONS.votes[wep] + 1
			end
		end
	end
	
	--WEAPONS.DBGVoteTable()
end

function WEAPONS:PickWinners()
	local winners = {}
	
	--find the max store that
	--then the next max repeat
	local last_max = 1000000
	local num_winners = WEAPONS.GetPoolSize()
	local pool_remaining = num_winners
	
	while pool_remaining > 0 do
		local loop_max = -1
		local loop_pool = {} --the loop pool contains weapons with the same number of votes
		
		for wep, v in pairs(WEAPONS.votes) do
			if v > loop_max and v < last_max then
				loop_pool = {}
				loop_pool[wep] = true
				loop_max = v
			elseif v == loop_max then
				loop_pool[wep] = true
			else
			end
		end
		
		last_max = loop_max
		
		--draw from the loop_pool, selecting randomly if need be
		local sub_pool, sub_pool_size = GetRandomSubPool(loop_pool, pool_remaining)
		for wep, _ in pairs(sub_pool) do
			winners[wep] = true --a winner is you!
		end
		pool_remaining = pool_remaining - sub_pool_size
	end
	
	WEAPONS.winners = winners
	WEAPONS.winners_length = num_winners
end

function GetRandomSubPool(loop_pool, pool_remaining)
	local result_pool = {}
	local loop_pool_size = 0
	for a,b in pairs(loop_pool) do loop_pool_size = loop_pool_size + 1 end
	local pool_used = 0
	
	if loop_pool_size > pool_remaining then
		--pull from pool randomly
		local loop_pool_keys = {}
		local n = 0
		for k,v in pairs(loop_pool) do
			n = n + 1
			loop_pool_keys[n] = k
		end
		
		local remain = pool_remaining
		while remain > 0 do
			local wep = table.remove(loop_pool_keys, math.random(#loop_pool_keys))
			result_pool[wep] = true
			remain = remain - 1
		end
		loop_pool_size = pool_remaining
	else
		--just put the items in and return how many we put
		result_pool = loop_pool
	end
	
	return result_pool, loop_pool_size
end

function WEAPONS.PlayerDisconnected(ply)
	WEAPONS.player_votes[ply] = nil
end

local function VoteForWeapon(ply, cmd, args)
	--ignore votes if player has too many
	if WEAPONS.PlayerAboveVoteLimit(ply) then return end
	--ignore bad params
	if args[1] == nil then return end
	
	if WEAPONS.all[args[1]] == nil then return end
	
	--on client this looks like RunConsoleCommand("_dd_vote_for_weapon", "weapon class name")
	WEAPONS.GetVoteListForPlayer(ply)[args[1]] = true
	
	if GetConVar("dd_start_on_votes_in"):GetBool() then
		MaybeStartRoundEarly()
	end
end
concommand.Add("_dd_vote_for_weapon", VoteForWeapon)

local function ClearWeaponVotes(ply, cmd, args)
	WEAPONS.player_votes[ply] = {}
end
concommand.Add("_dd_clear_weapon_votes", ClearWeaponVotes)

local function SelectWeapon(ply, cmd, args)
	mid_switch = GetConVar("dd_mid_round_switching"):GetBool()
	if not IsValid(ply) then return end
	--we spawn players on the rising edge of ready, whether they like it or not
	local ready_before = ply:IsLoadoutReady() or false
	
	--if dd_mid_round_switching is 1, allow switching, otherwise stop if already ready
	if ready_before and !mid_switch then return end
	--some validity checks
	
	if args[1] == nil then return end
	
	local wep = args[1]
	
	if not WEAPONS.WeaponExists(wep) then return end
	if not WEAPONS.IsAWinner(wep) then return end
	
	ply.weapon_loadout[wep] = true
	
	ply:CalculateLoadoutReady()
	
	local ready_after = ply:IsLoadoutReady()
	
	if (not ready_before and ready_after) or mid_switch then
		--player is ready to spawn
		--set ready and tick timer to go!
		--also, block player from modifying loadout by buffering the loadout
		ply.weapon_loadout_buffered = {}
		for w,_ in pairs(ply.weapon_loadout) do
			ply.weapon_loadout_buffered[w] = true
		end
	end
end
concommand.Add("_dd_select_weapon", SelectWeapon)

local function ClearWeaponSelection(ply, cmd, args)
	--we dont change ready, to prevent exploits
	--also prevents players from changing loadout in the match
	ply.weapon_loadout = {}
end
concommand.Add("_dd_clear_weapon_selection", ClearWeaponSelection)


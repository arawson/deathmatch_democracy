--????

surface.CreateFont( "ScoreboardDefault", {
	font		= "Helvetica",
	size		= 22,
	weight		= 800
})

surface.CreateFont( "ScoreboardDefaultTitle", {
	font		= "Helvetica",
	size		= 32,
	weight		= 800
})

local colorize = {
	admin = Color(220, 180, 0, 225),
	background = Color(90, 90, 150, 190),
	plistback = Color(90, 90, 90, 128)
};

--note to self
--going for a contact card look
--[[
/-----------------------\
|         Name          |
|PICPICPICPIC|Mute     ?| --thats supposed to be a checkbox
|PICPICPICPIC|Kills =   |
|PICPICPICPIC|Deaths=   |
|PICPICPICPIC|Ping  =   |
|PICPICPICPIC|          |
|Maybe some flavor text?|
\-----------------------/
]]

--similar to base's player line, but a little bit different of a look
local PLAYER_TILE = {
	Init = function( self )
		self:SetSize(300, 224)
		
		self.Avatar = self:Add("AvatarImage")
		self.Avatar:SetSize(184,184)
		self.Avatar:SetPos(10,30)
		
		self.Name = self:Add("DLabel")
		self.Name:SetSize(400,30)
		self.Name:SetPos(10,0)
		self.Name:SetFont("ScoreboardDefault")
		
		self.Mute = self:Add("DButton")
		self.Mute:SetSize(76, 30)
		self.Mute:SetText("Mute")
		self.Mute:SetPos(204, 30)
		
		self.KillsLabel = self:Add("DLabel")
		self.KillsLabel:SetText("Kills")
		self.KillsLabel:SetPos(204, 70)
		self.KillsLabel:SetFont("ScoreboardDefault")
		self.KillsLabel:SetSize(76, 30)
		
		self.Kills = self:Add("DLabel")
		self.Kills:SetPos(204, 85)
		self.Kills:SetFont("ScoreboardDefault")
		self.Kills:SetSize(76, 30)
		
		self.DeathsLabel = self:Add("DLabel")
		self.DeathsLabel:SetText("Deaths")
		self.DeathsLabel:SetPos(204, 115)
		self.DeathsLabel:SetFont("ScoreboardDefault")
		self.DeathsLabel:SetSize(76, 30)
		
		self.Deaths = self:Add("DLabel")
		self.Deaths:SetPos(204, 130)
		self.Deaths:SetFont("ScoreboardDefault")
		self.Deaths:SetSize(76, 30)
		
		self.PingLabel = self:Add("DLabel")
		self.PingLabel:SetText("Ping")
		self.PingLabel:SetPos(204, 160)
		self.PingLabel:SetFont("ScoreboardDefault")
		self.PingLabel:SetSize(76, 30)
		
		self.Ping = self:Add("DLabel")
		self.Ping:SetPos(204, 175)
		self.Ping:SetFont("ScoreboardDefault")
		self.Ping:SetSize(76, 30)
	end,
	
	Setup = function(self, pl)
		self.Player = pl
		self.Avatar:SetPlayer(pl)
		self:Think(self)
	end,
	
	Think = function(self)
		if (!IsValid(self.Player)) then
			self:Remove()
			return
		end
		
		if (self.PName == nil || self.PName != self.Player:Nick()) then
			self.PName = self.Player:Nick()
			self.Name:SetText(self.PName)
		end
		
		if (self.NumKills == nil || self.NumKills != self.Player:Frags()) then
			self.NumKills = self.Player:Frags()
			self.Kills:SetText(self.NumKills)
		end
		
		if (self.NumDeaths == nil || self.NumDeaths != self.Player:Deaths()) then
			self.NumDeaths = self.Player:Deaths()
			self.Deaths:SetText(self.NumDeaths)
		end
		
		if (self.NumPing == nil || self.NumPing != self.Player:Deaths()) then
			self.NumPing = self.Player:Ping()
			self.Ping:SetText(self.NumPing)
		end
		
		if (self.Muted == nil || self.Muted != self.Player:IsMuted()) then
			self.Muted = self.Player:IsMuted()
			if (self.Muted) then
				self.Mute:SetText("Unmute")
			else
				self.Mute:SetText("Mute")
			end
			self.Mute.DoClick = function() self.Player:SetMuted(!self.Muted) end
		end
		
		------??>???????
		if (self.Player:Team() == TEAM_CONNECTING) then
			self.SetZPos(2000)
		end
		--the base cl_scoreboard discusses the limitations of this method:
		-- This is what sorts the list. The panels are docked in the z order,
		-- so if we set the z order according to kills they'll be ordered that way!
		-- Careful though, it's a signed short internally, so needs to range between -32,768k and +32,767
		self:SetZPos( (self.NumKills * -50) + self.NumDeaths )
	end,
	
	Paint = function(self, w, h)
		if (!IsValid(self.Player)) then
			return
		end
		
		--draw background color based on player status
		if ( self.Player:Team() == TEAM_CONNECTING ) then
			draw.RoundedBox( 4, 0, 0, w, h, Color( 100, 100, 100, 200 ) )
			return
		end

		if  ( !self.Player:Alive() ) then
			draw.RoundedBox( 4, 0, 0, w, h, Color( 115, 100, 100, 255 ) )
			return
		end

		if ( self.Player:IsAdmin() ) then
			draw.RoundedBox( 4, 0, 0, w, h, Color( 115, 130, 115, 255 ) )
			return
		end

		draw.RoundedBox( 4, 0, 0, w, h, Color( 115, 115, 115, 255 ) )
	end
}

--convert that table into a panel derived from dpanel
PLAYER_TILE = vgui.RegisterTable(PLAYER_TILE, "DPanel")

local SCORE_BOARD = {
	Init = function(self)
		self.Header = self:Add("Panel")
		self.Header:Dock(TOP)
		self.Header:SetHeight(100)
		
		self.Name = self.Header:Add("DLabel")
		self.Name:SetFont("ScoreboardDefaultTitle")
		self.Name:SetTextColor(Color(255,255,255,255))
		self.Name:Dock(TOP)
		self.Name:SetHeight(40)
		self.Name:SetContentAlignment(5)
		self.Name:SetExpensiveShadow(2, Color(0,0,0,200))
		
		self.ScoreScroll = self:Add("DScrollPanel")
		self.ScoreScroll:Dock(FILL)
		
		self.Scores = self.ScoreScroll:Add("DIconLayout")
		self.Scores:SetSpaceY(5)
		self.Scores:SetSpaceX(5)
		self.Scores:Dock(FILL)
	end,
	
	PerformLayout = function(self)
		self:SetSize(700, ScrH() - 200)
		self:SetPos(ScrW() / 2 - 350, 100)
	end,
	
	Paint = function(self, w, h)
		--nothing???
	end,
	
	Think = function(self, w, h)
		self.Name:SetText("Deathmatch Democracy: " .. GetHostName())
		
		local plyrs = player.GetAll()
		for id, pl in pairs(plyrs) do
			if (IsValid(pl.ScoreEntry)) then continue end
			
			pl.ScoreEntry = vgui.CreateFromTable(PLAYER_TILE, pl.ScoreEntry)
			pl.ScoreEntry:Setup(pl)
			
			self.Scores:Add(pl.ScoreEntry)
		end
	end
}

SCORE_BOARD = vgui.RegisterTable(SCORE_BOARD, "EditablePanel")

function GM:ScoreboardShow()
	if (!IsValid(g_Scoreboard)) then
		g_Scoreboard = vgui.CreateFromTable(SCORE_BOARD)
	end
	
	if (IsValid(g_Scoreboard)) then
		g_Scoreboard:Show()
		g_Scoreboard:MakePopup()
		g_Scoreboard:SetKeyboardInputEnabled(false)
	end
end

function GM:ScoreboardHide()
	if (IsValid(g_Scoreboard)) then
		g_Scoreboard:Hide()
	end
end

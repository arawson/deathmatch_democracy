include("shared.lua")

include("util.lua")
include("player_ext.lua")
include("weapon_voting_shd.lua")
include("cl_weapon_voting.lua")
include("cl_hud.lua")
include("cl_gui.lua")
include("cl_scoreboard.lua")
include("player_class/class_deathmatcher.lua")

DEFINE_BASECLASS("gamemode_base")

function GM:Initialize()
	self.BaseClass:Initialize()
	
	print("CLIENT: GM:Initialize")
	
	WEAPONS.Initialize()
	CL_GUI.Initialize()
	
	GAMEMODE.round_state = ROUND_WAIT
	GAMEMODE.match_round = -1
	GAMEMODE.client_ready = true
end


function GetRoundState() return GAMEMODE.round_state end

local function RoundStateChange(o, n)
	if n == ROUND_WAITING then
	elseif n == ROUND_WEP_VOTING then
		CL_GUI.ClearSelection()
		CL_GUI.ClearVotes()
		CL_GUI.WeaponVotingPopup()
	elseif n == ROUND_ACTIVE then
		WEAPONS.UpdateActive()
		CL_GUI.UpdateActive()
		CL_GUI.WeaponSelectionPopup()
	elseif n == ROUND_POST then
	else
		print("DOGSDOGSDOGSDOGSDOGSDOGS")
	end
end

local function ReceiveRoundState()
	local o = GetRoundState()
	GAMEMODE.round_state = net.ReadUInt(3)
	
	if o != GAMEMODE.round_state then
		RoundStateChange(GAMEMODE.round_state, GAMEMODE.round_state)
	end
end
net.Receive("DD_RoundState", ReceiveRoundState)

local function ReceiveMatchRound()
	GAMEMODE.match_round = net.ReadUInt(32)
	local match_round = GAMEMODE.match_round
	print ("match round is " .. GAMEMODE.match_round)
end
net.Receive("DD_MatchRound", ReceiveMatchRound)

local function ReceiveSpawned()
	CL_GUI.PlayerSpawnForBattle()
end
net.Receive("DD_Spawned", ReceiveSpawned)


DeriveGamemode("base")

GM.Name = "Deathmatch Democracy"
GM.Author = "thebinaryblob"
GM.Email = "superlegokid@gmail.com"
GM.Website = "N/A"
GM.Help = "Voting Deathmatch"
GM.Version = "0.1 Alpha"

GM.SelectModel = true
GM.SelectColor = true

--round status constants
ROUND_WAITING = 1
ROUND_WEP_VOTING = 2
ROUND_ACTIVE = 3
ROUND_POST = 4

include( "util.lua" )

local math = math

function DisableNoClip(ply)
	return ply:IsAdmin() or ply:IsSuperAdmin()
	--return false
end
hook.Add("PlayerNoClip", "DisableNoClip", DisableNoClip)

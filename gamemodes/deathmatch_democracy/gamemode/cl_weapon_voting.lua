
WEAPONS.client_votes = {}
WEAPONS.client_vote_count = 0

WEAPONS.client_selection = {}
WEAPONS.client_selection_count = 0

function WEAPONS.InitializeClient()
	for wep,_ in pairs(WEAPONS.all) do
		client_votes[wep] = false
	end
end

--no voting or selection feedback right now
--only enablement by server
local function ReceiveWinningWeapons()
	WEAPONS.winners_length = net.ReadInt(32)
	WEAPONS.winners = {}
	print(WEAPONS.winners_length)
	for i = 1, WEAPONS.winners_length do
		local wep = net.ReadString()
		print ("wep for round " .. wep)
		WEAPONS.winners[wep] = true
	end
end
net.Receive("DD_WinningWeapons", ReceiveWinningWeapons)

function WEAPONS.ToggleVoteSelection(ClassName)
	if WEAPONS.client_votes[ClassName] then
		WEAPONS.client_votes[ClassName] = false
		WEAPONS.client_vote_count = WEAPONS.client_vote_count - 1
	else
		if WEAPONS.client_vote_count >= GetConVarNumber("dd_max_weapons") then
			--do nothing
			--the server will just ignore extras
		else
			WEAPONS.client_votes[ClassName] = true
			WEAPONS.client_vote_count = WEAPONS.client_vote_count + 1
		end
	end
end

function WEAPONS.ClientClearVotes()
	WEAPONS.client_votes = {}
	WEAPONS.client_vote_count = 0
end

function WEAPONS.ClientVotedFor(ClassName)
	return WEAPONS.client_votes[ClassName]
end

function WEAPONS.ToggleSelection(ClassName)
	print("toggle selection")
	if WEAPONS.client_selection[ClassName] then
		print(ClassName .. " is already selected, deselect ")
		WEAPONS.client_selection[ClassName] = false
		WEAPONS.client_selection_count = WEAPONS.client_selection_count - 1
	else
		print ("not already selected")
		if WEAPONS.client_selection_count >= GetConVarNumber("dd_max_weapons") then
			print ("already at max selection")
			--do nothing
			--the server will just ignore extras
		else
			print ("not at max selection, select")
			WEAPONS.client_selection[ClassName] = true
			WEAPONS.client_selection_count = WEAPONS.client_selection_count + 1
		end
	end
end

function WEAPONS.ClientClearSelection()
	WEAPONS.client_selection = {}
	WEAPONS.client_selection_count = 0
end

function WEAPONS.ClientHasSelected(ClassName)
	return WEAPONS.client_selection[ClassName]
end

AddCSLuaFile("cl_init.lua")
AddCSLuaFile("cl_gui.lua")
AddCSLuaFile("cl_hud.lua")
AddCSLuaFile("cl_scoreboard.lua")
AddCSLuaFile("shared.lua")
AddCSLuaFile("util.lua")
AddCSLuaFile("player_class/class_deathmatcher.lua")
AddCSLuaFile("player_ext.lua")
AddCSLuaFile("admin.lua")
AddCSLuaFile("weapon_voting.lua")
AddCSLuaFile("weapon_voting_shd.lua")
AddCSLuaFile("cl_weapon_voting.lua")
AddCSLuaFile("match.lua")

include("shared.lua")

include("match.lua")
include("player_ext.lua")
include("player_class/class_deathmatcher.lua")
include("admin.lua")
include("weapon_voting_shd.lua")
include("weapon_voting.lua")

DEFINE_BASECLASS("gamemode_base")

util.AddNetworkString("DD_RoundState")
util.AddNetworkString("DD_MatchRound")
util.AddNetworkString("DD_WinningWeapons")
util.AddNetworkString("DD_Spawned")

function GM:Initialize()
	self.BaseClass:Initialize()
	MsgN("Deathmatch Democracy ready to be counted!")
	MsgN("0.1 Alpha")
	
	--force friendly fire to be enabled, if it is off we do not get lag compensation
	RunConsoleCommand("mp_friendlyfire", "1")
	
	--delay reading of cvars until config has definitely loaded?
	--ttt does this, hope its right
	GAMEMODE.cvar_init = false
	
	math.randomseed(os.time())
	
	SetGlobalFloat("dd_round_end", -1)
	
	--now we load weapons on the server side
	WEAPONS.Initialize()
	
	GAMEMODE.cvar_init = false
	
	SetRoundState(ROUND_WAITING)
	SetMatchRound(1)
	
	WaitForPlayers()
end

function GM:PlayerDisconnected(ply)
	print(ply:Nick() .. " has disconnected")
	WEAPONS.PlayerDisconnected(ply)
end

function GM:PlayerSpawn(ply)
--mike hunt
	MsgN(ply:Nick() .. " is on the hunt!")
	
	net.Start("DD_Spawned")
	net.Send(ply)
	
	player_manager.SetPlayerClass(ply, "player_deathmatcher")
	BaseClass.PlayerSpawn(self, ply)
end

--the same workaround ttt uses for broken convar replication
function GM:SyncGlobals()
	print("sync globals, stub!")
end

function GM:InitCvars()
	print("deathmatch initializing convar settings")
	GAMEMODE:SyncGlobals()
	self.cvar_init = true
end

function SetMatchRound(round)
	GAMEMODE.match_round = round
	SendMatchRound(round)
end

function GetMatchRound()
	return GAMEMODE.match_round
end

function SendMatchRound(round, ply)
	net.Start("DD_MatchRound")
		net.WriteUInt(round, 32)
	return ply and net.Send(ply) or net.Broadcast()
end

function SetRoundState(state)
	GAMEMODE.round_state = state
	SendRoundState(state)
end

function GetRoundState()
	return GAMEMODE.round_state
end

function SendRoundState(state, ply)
	net.Start("DD_RoundState")
		net.WriteUInt(state, 3)
	return ply and net.Send(ply) or net.Broadcast()
end

function EnoughPlayers()
	local ready = 0
	--ttt says: only count truly available players
	for _, ply in pairs(player.GetAll()) do
		if IsValid(ply) then
			ready = ready + 1
		end
	end
	
	return ready >= 1
end

function GM:MDDelayRoundStartForVote()
	--gm13 doesnt implement a voting system, so i must implement it myself
	return false
end

function BeginRoundVote()
	local delay_round, delay_length = hook.Call("MDDelayRoundStartForVote")
	
	for k, ply in pairs(player.GetAll()) do
		if IsValid(ply) then
			ply:SetFrags(0)
			ply:SetDeaths(0)
		end
	end
	
	if delay_round then
		delay_length = delay_length or 30
		
		timer.Create("delayedprep", delay_length, 1, BeginRoundVote)
		return
	end
	
	--CleanUp()
	
	--TODO: initialize data for the round
	--TODO: also reset kills on the first round of a match
	
	--schedule round start
	local ptime = GetConVarNumber("dd_weapon_voting_time")
	print("SERVER: set state to voting, and add time")
	SetRoundState(ROUND_WEP_VOTING)
	SetRoundEnd(CurTime() + ptime)
	
	timer.Create("vote2begin", ptime, 1, BeginRound)
end

function SetRoundEnd(endtime)
	SetGlobalFloat("dd_round_end", endtime)
end

function WaitForPlayersChecker()
	ForceSpawnPlayers(false)
	print("checking if we have enough players")
	if GetRoundState() == ROUND_WAITING then
		if EnoughPlayers() then
			print("have enough players, start voting")
			timer.Create("wait2vote", 1, 1, BeginRoundVote)
			timer.Stop("waitingforply")
		end
	end
end

function WaitForPlayers()
	print("wating for players")
	SetRoundState(ROUND_WAITING)
	if not timer.Start("waitingforply") then
		timer.Create("waitingforply", 2, 0, WaitForPlayersChecker)
	end
end

local function SpawnTicker()
	for k, ply in pairs(player.GetAll()) do
		if IsValid(ply) then
			if ply:IsLoadoutReady() and GAMEMODE.round_state == ROUND_ACTIVE and (not ply:Alive() or ply:IsSpectator()) then
				if !ply:HasSpawnedThisRound() then
					ply:SpawnForRound()
				end
			end
		end
	end
end

function StartSpawnTicker()
	timer.Create("spawnticker", 1, 0, SpawnTicker)
end

function StopSpawnTicker()
	timer.Stop("spawnticker")
end

function ForceSpawnPlayers(round)
	print("force spawn players")
	for k, ply in pairs(player.GetAll()) do
		if IsValid(ply) then
			if round then 
				ply:SpawnForRound()
			else
				ply:SpawnForOther()
			end
		end
	end
end

local function InitRoundEndTime()
	--init round values
	local endtime = CurTime() + (GetConVarNumber("dd_round_length")*60)
	SetRoundEnd(endtime)
end

local function WinChecker()
	if GetRoundState() == ROUND_ACTIVE then
		if CurTime() > GetGlobalFloat("dd_round_end", 0) then
			EndRound()
		end
		--we dont have a team win condition
	end
end

function StartWinChecks()
	if not timer.Start("winchecker") then
		timer.Create("winchecker", 1, 0, WinChecker)
	end
end

function StopWinChecks()
	timer.Stop("winchecker")
end

function StopRoundTimers()
	timer.Stop("wait2vote")
	timer.Stop("vote2begin")
	timer.Stop("end2vote") --end of match
	timer.Stop("end2begin") --end of round not end of match
	timer.Stop("winchecker")
end

function CheckForAbort()
	if not EnoughPlayers() then
		StopRoundTimers()
		WaitForPlayers()
		return true
	end
	
	return false
end

local function ClearPlayerLoadouts()
	for k, ply in pairs(player.GetAll()) do
		if IsValid(ply) then
			ply:InitWeaponLoadout()
		end
	end
end

function BeginRound()
	timer.Stop("vote2begin")
	GAMEMODE:SyncGlobals()
	if CheckForAbort() then return end
	InitRoundEndTime()
	if CheckForAbort() then return end
	StartSpawnTicker()
	if CheckForAbort() then return end
	
	print("round starting")
	WEAPONS.Recount()
	WEAPONS.PickWinners()
	SendWinningWeapons()
	ClearPlayerLoadouts()
	StartWinChecks()
	GAMEMODE.DamageLog = {}
	GAMEMODE.RoundStartTime = CurTime()
	SetRoundState(ROUND_ACTIVE)
end

function EndRound()
	ForceSpawnPlayers(false)
	--print results?
	SetRoundState(ROUND_POST)
	
	local ptime = GetConVarNumber("dd_round_post_time")
	local round_limit = GetConVarNumber("dd_match_rounds")
	
	if round_limit > GetMatchRound() then
		print("not at round limit")
		SetMatchRound(GetMatchRound() + 1)
		timer.Create("end2begin", ptime, 1, BeginRound)
	else
		print("at match limit, start new match")
		SetMatchRound(1)
		timer.Create("end2vote", ptime, 1, BeginRoundVote)
	end
	
	--ttt says piggyback on "round end" time global variable to show end of phase timer
	SetRoundEnd(CurTime() + ptime)
	
	StopWinChecks()
	
	CheckForMapSwitch()
	
	--may want to deal with scoring here
end

local function ForceRoundRestart(ply, command, args)
	if not IsValid(ply) then return end
	
	if ply:IsAdmin() or ply:IsSuperAdmin() or cvars.Bool("sv_cheats", 1) then
		print("round restart")
		ClearPlayerLoadouts()
		StopRoundTimers()
		ForceSpawnPlayers(false)
		BeginRoundVote()
	else
		ply:PrintMessage(HUD_PRINTCONSOLE, "You must be admin or super admin to use this command")
	end
end
concommand.Add("dd_roundrestart", ForceRoundRestart)

function CheckForMapSwitch()
end

function SendWinningWeapons(ply)
	net.Start("DD_WinningWeapons")
		net.WriteUInt(WEAPONS.winners_length, 32)
		for wep,_ in pairs(WEAPONS.winners) do
			print (wep .. " is a winner")
			--this is limited to 1024 characters
			--if you have a weapon name longer than 
			net.WriteString(wep)
		end
	return ply and net.Send(ply) or net.Broadcast()
end

function GM:PostPlayerDeath(ply)
	ply:SetDead(true)
	--not dealing with changes to respawn time during round for simplicities sake
	timer.Simple(GetConVarNumber("dd_respawn_time"), function()
		if IsValid(ply) then
			if GAMEMODE.round_state == ROUND_ACTIVE then
				ply:SpawnForRound()
			else
				ply:SpawnForOther()
			end
		end
	end)
end

--called by weapon_voting iff dd_start_on_votes_in is true
function MaybeStartRoundEarly()
	ready = true
	for k, ply in pairs(player.GetAll()) do
		if IsValid(ply) then
			if WEAPONS.PlayerBelowVoteLimit(ply) then
				ready = false
			end
		end
	end
	
	if ready and GAMEMODE.round_state == ROUND_WEP_VOTING then
		BeginRound()
	end
end

function GM:ShowSpare1(ply)
	ply:ConCommand("dd_cl_weapon_vote_popup")
end

function GM:ShowSpare2(ply)
	ply:ConCommand("dd_cl_weapon_selection_popup")
end

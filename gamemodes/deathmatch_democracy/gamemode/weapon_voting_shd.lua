
WEAPONS = {}

--an array of the weapons that are allowed this round
WEAPONS.active = {}

--eg to block the dear sister swep
WEAPONS.blacklist = {}

--a table of tables of the weapons that are available for voting by category
WEAPONS.categorised = {}

--a list of weapons available
WEAPONS.all = {}

--map<Weapon:ClassName, int> tally of votes
WEAPONS.votes = {}

--count of player votes
--map<Player, Map<Weapon:ClassName, bool>> easy size checks!
WEAPONS.player_votes = {}

--just a list of the weapons that won
WEAPONS.winners = {}
WEAPONS.winners_length = 0

function WEAPONS.Initialize()
	local weps = list.Get("Weapon")
	local categorised = {}
	local all = {}
	local votes = {}
	
	for k, w in pairs(weps) do
		if not w.Spawnable then continue end
		
		--i dont think its possible to read data from built-in weapons
		-- these arent weapon class objects, they are something else
		--so skip built in weapons
		if weapons.Get(w.ClassName) == nil then continue end
		
		categorised[w.Category] = categorised[w.Category] or {}
		categorised[w.Category][w] = true
		--table.insert(categorised[w.Category], w)
		table.insert(all, w.ClassName)
		votes[w.ClassName] = 0
		all[w.ClassName] = true
	end
	
	WEAPONS.categorised = categorised
	WEAPONS.all = all
	WEAPONS.votes = votes
end

function WEAPONS.DBGVoteTable()
	print("printing weapon vote table")
	for wep, v in pairs(WEAPONS.votes) do
		print(wep .. ": " .. v)
	end
	print("done printing vote table")
end

function WEAPONS.DBGPlayerVoteList(ply)
	print ("ply vote list")
	for wep, _ in pairs(WEAPONS.GetVoteListForPlayer(ply)) do
		print("ply with " .. wep)
	end
end

function WEAPONS.DBGAllList()
	print ("print all weapons detected")
	local weps = ""
	for wep, _ in pairs(WEAPONS.all) do
		weps = weps .. " " .. wep
	end
	print (weps)
end
concommand.Add("dd_debug_weapon_list", WEAPONS.DBGAllList)

function WEAPONS.GetVoteListForPlayer(ply)
	if ply == nil then return nil end --have fun with that
	if WEAPONS.player_votes[ply] == nil then
		WEAPONS.player_votes[ply] = {}
	end
	return WEAPONS.player_votes[ply]
end

function WEAPONS.GetPoolSize()
	if GetConVar("dd_no_selection_mode"):GetBool() then
		return math.min(GetConVarNumber("dd_max_weapons"), WEAPONS.GetNumWeapons())
	else
		return math.min(GetConVarNumber("dd_weapon_pool_size"), WEAPONS.GetNumWeapons())
	end
end

function WEAPONS.GetNumWeapons()
	local value = 0
	for k,v in pairs(WEAPONS.all) do value = value + 1 end
	return value
end

function WEAPONS.WeaponExists(wep)
	for w,_ in pairs(WEAPONS.all) do
		if wep == w then
			return true
		end
	end
	return false
end

function WEAPONS.IsAWinner(wep)
	for w,_ in pairs(WEAPONS.winners) do
		if wep == w then return true end
	end
	return false
end


function WEAPONS.UpdateActive()
	WEAPONS.active = {}
	for w,_ in pairs(WEAPONS.winners) do
		WEAPONS.active[w] = true
	end
end

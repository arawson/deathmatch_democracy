
DEFINE_BASECLASS("player_default")

local PLAYER = {}

PLAYER.DisplayName = "Model Citizen"
PLAYER.WalkSpeed = 200
PLAYER.RunSpeed = 300
--fix jump power so players dont have to do jump tricks as much
PLAYER.JumpPower = 250
PLAYER.CanUseFlashlight = true

function PLAYER:Init()
	self.Player:InitWeaponLoadout()
end

function PLAYER:Spawn()
	if not self.Player:CanSpawn() or GAMEMODE.round_state != ROUND_ACTIVE then
		self.Player:Spectate(OBS_MODE_ROAMING)
	else
		self.Player:UnSpectate()
	end
end

function PLAYER:Loadout()
	self.Player:RemoveAllAmmo()
end

player_manager.RegisterClass("player_deathmatcher", PLAYER, "player_default")

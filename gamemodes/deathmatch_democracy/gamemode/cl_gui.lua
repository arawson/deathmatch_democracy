
CL_GUI = {}

CL_GUI.UnSelectedColor = Color(128,128,128,190)
CL_GUI.SelectedColor = Color(128,180,128,190)

function CL_GUI.Initialize()
	CL_GUI.MakeVotingPopup()
	CL_GUI.MakeSelectionPopup()
end

local function SetImageOn(imageButton, wepclass)
	local wep = weapons.Get(wepclass)
	local name = "entities/" .. wep.ClassName .. ".png"
	local mat = Material(name)
	
	--sandbox says look for the old style material
	if (!mat or mat:IsError()) then
		name = name:Replace("entities/", "VGUI/entities/")
		name = name:Replace(".png", "")
		mat = Material(name)
	end
	
	if (!mat or mat:IsError()) then
		if (wep.WepSelectIcon != nil) then
			local tex = wep.WepSelectIcon
			local texdata = {}
			texdata.texture = tex
			texdata.w = surface.GetTextureSize(tex)
			texdata.h = texdata.w
			texdata.x = 0
			texdata.y = 0
			imageButton.Paint = function()
				draw.TexturedQuad(texdata)
				draw.SimpleText("Missing Image", "Default", 0, 0, Color(255,0,0,255))
			end
		end
		return
	end
	
	imageButton:SetMaterial(name)
end

function CL_GUI.MakeVotingPopup()
	CL_GUI.vote = {}
	local vote = CL_GUI.vote
	
	local width = ScrW() * 0.9
	local height = ScrH() * 0.9
	local margin_h = 10
	local margin_v = 5
	local tbar_h = 25
	
	local frame = vgui.Create("DFrame")
	vote.frame = frame
	frame:SetDraggable(false)
	frame:ShowCloseButton(true)
	frame:SetDeleteOnClose(false)
	frame:SetVisible(false)
	frame:SetPos(ScrW() * 0.05, ScrH() * 0.05)
	frame:SetSize(width, height)
	frame:SetTitle("Vote for Weapons!")
	
	local submit = vgui.Create("DButton", frame)
	vote.submit = submit
	submit:SetText("Place Your Votes!")
	submit:SetSize(250, 30)
	submit:SetPos(width - 250 - margin_h, height - 30 - margin_v)
	submit.DoClick = CL_GUI.SubmitVotes
	
	local clear = vgui.Create("DButton", frame)
	vote.clear = clear
	clear:SetText("Clear Your Votes!")
	clear:SetSize(250, 30)
	clear:SetPos(width - 250 * 2 - margin_h * 2, height - 30 - margin_v)
	clear.DoClick = CL_GUI.ClearVotes
	
	local categories = vgui.Create("DTree", frame)
	vote.categories = categories
	categories:SetPos(margin_h, margin_v + tbar_h)
	categories:SetSize(250, height - margin_v * 2 - tbar_h)
	categories.OnNodeSelected = CL_GUI.CategorySelected
	
	local categoriessortable = {}
	local n = 0
	for category,_ in pairs(WEAPONS.categorised) do
		n = n + 1
		categoriessortable[n] = category
	end
	for category,_ in SortedPairs(WEAPONS.categorised) do
		local node = CL_GUI.vote.categories:AddNode(category, "icon16/gun.png")
	end
	
	local wep_scroller = vgui.Create("DScrollPanel", frame)
	vote.wep_scroller = wep_scroller
	wep_scroller:SetPos(margin_h + 250 + margin_h, margin_v + tbar_h)
	wep_scroller:SetSize(width - 250 - margin_h - 20, height - 30 - margin_v * 3 - tbar_h)
	wep_scroller:SetBackgroundColor(Color(128,128,128,128))
	
	--create the category panels
	--category_panels is map<category, t> | t = {panel, weapons}
	vote.category_panels = {}
	for cata,weplist in pairs(WEAPONS.categorised) do
		wep_panel = vgui.Create("DIconLayout", wep_scroller)
		wep_panel:SetBackgroundColor(Color(128,128,128,128))
		wep_panel:SetSize(width - 250 - margin_h * 2 - 20, 200)
		
		vote.category_panels[cata] = {}
		t = vote.category_panels[cata]
		t.panel = wep_panel
		t.weapons = {}
		weplistsortable = {}
		local n = 0
		for wep,_ in pairs(weplist) do
			n = n + 1
			weplistsortable[n] = wep
		end
		
		for _,wep in SortedPairsByMemberValue(weplistsortable, "PrintName") do
			local pnal = wep_panel:Add("DPanel")
			pnal.Weapon = wep
			pnal:SetSize(150,190)
			pnal:SetBackgroundColor(CL_GUI.UnSelectedColor)
			
			local button = vgui.Create("DImageButton", pnal)
			button:SetPos(5,5)
			button:SetSize(140, 140)
			button:SetKeepAspect(true)
			SetImageOn(button, wep.ClassName)
			button.DoClick = function()
				print("selected " .. wep.ClassName)
				WEAPONS.ToggleVoteSelection(wep.ClassName)
				if (WEAPONS.ClientVotedFor(wep.ClassName)) then
					pnal:SetBackgroundColor(CL_GUI.SelectedColor)
				else
					pnal:SetBackgroundColor(CL_GUI.UnSelectedColor)
				end
			end
			
			local label = vgui.Create("DLabel", pnal)
			label:SetPos(5, 145)
			label:SetText(wep.PrintName)
			label:SetSize(140, 40)
		end
		
		wep_panel:SetVisible(false)
	end
end

function CL_GUI.SubmitVotes()
	LocalPlayer():ConCommand("_dd_clear_weapon_votes")
	for wep_class,yes in pairs(WEAPONS.client_votes) do
		if yes then
			LocalPlayer():ConCommand("_dd_vote_for_weapon " .. wep_class)
		end
	end
end

function CL_GUI.ClearVotes()
	WEAPONS.ClientClearVotes()
	LocalPlayer():ConCommand("_dd_clear_weapon_votes")
	
	local vote = CL_GUI.vote
	for cata,t in pairs(vote.category_panels) do
		print ("clear cata panel " .. cata)
		for _,pnal in pairs(t.panel:GetChildren()) do
			pnal:SetBackgroundColor(CL_GUI.UnSelectedColor)
		end
	end
end

--listener for tree in voting panel
function CL_GUI.CategorySelected(a,node)
	--for some reason, variables get confused on this callback
	local node = CL_GUI.vote.categories:GetSelectedItem()
	
	if node.WepPanel then return end
	
	for cat_panel, t in pairs(CL_GUI.vote.category_panels) do
		t.panel:SetVisible(false)
	end
	
	t = CL_GUI.vote.category_panels[node:GetText()]
	t.panel:SetVisible(true)
end

function CL_GUI.MakeSelectionPopup()
	CL_GUI.selection = {}
	local selection = CL_GUI.selection
	
	local width = ScrW() * 0.9
	local height = ScrH() * 0.9
	local margin_h = 10
	local margin_v = 5
	local tbar_h = 25
	
	local frame = vgui.Create("DFrame")
	selection.frame = frame
	frame:SetDraggable(false)
	frame:ShowCloseButton(true)
	frame:SetDeleteOnClose(false)
	frame:SetVisible(false)
	frame:SetPos(ScrW() * 0.05, ScrH() * 0.05)
	frame:SetSize(width, height)
	frame:SetTitle("Select Your Weapons!")
	
	local submit = vgui.Create("DButton", frame)
	selection.submit = submit
	submit:SetText("Make Your Selection!")
	submit:SetSize(250, 30)
	submit:SetPos(width - 250 - margin_h, height - 30 - margin_v)
	submit.DoClick = CL_GUI.SubmitSelection
	
	local clear = vgui.Create("DButton", frame)
	selection.clear = clear
	clear:SetText("Clear Your Selection!")
	clear:SetSize(250, 30)
	clear:SetPos(width - 250 * 2 - margin_h * 2, height - 30 - margin_v)
	clear.DoClick = CL_GUI.ClearSelection
	
	local wep_scroller = vgui.Create("DScrollPanel", frame)
	selection.wep_scroller = wep_scroller
	wep_scroller:SetPos(margin_h, margin_v + tbar_h)
	wep_scroller:SetSize(width - margin_h - 20, height - 30 - margin_v * 3 - tbar_h)
	wep_scroller:SetBackgroundColor(Color(128,128,128,128))
	
	local wep_panel = vgui.Create("DIconLayout", wep_scroller)
	selection.wep_panel = wep_panel
	wep_panel:SetBackgroundColor(Color(128,128,128,128))
	wep_panel:SetSize(width - 250 - margin_h * 2 - 20, 200)
	
	selection.sub_panels = {}
end

function CL_GUI.UpdateActive()
	for pan,_ in pairs(CL_GUI.selection.sub_panels) do
		pan:Remove()
	end
	
	CL_GUI.selection.sub_panels = {}
	
	WEAPONS.ClientClearSelection()
	
	print("put new")
	for wep_class,_ in pairs(WEAPONS.active) do
		local pnal = CL_GUI.selection.wep_panel:Add("DPanel")
		pnal.Weapon = wep_class
		pnal:SetSize(150,190)
		pnal:SetBackgroundColor(CL_GUI.UnSelectedColor)
		
		local button = vgui.Create("DImageButton", pnal)
		button:SetPos(5,5)
		button:SetSize(140, 140)
		button:SetKeepAspect(true)
		SetImageOn(button, wep_class)
		button.DoClick = function()
			print("selected " .. wep_class)
			WEAPONS.ToggleSelection(wep_class)
			if (WEAPONS.ClientHasSelected(wep_class)) then
				pnal:SetBackgroundColor(CL_GUI.SelectedColor)
			else
				pnal:SetBackgroundColor(CL_GUI.UnSelectedColor)
			end
		end
		
		local label = vgui.Create("DLabel", pnal)
		label:SetPos(5, 145)
		label:SetText(weapons.Get(wep_class).PrintName)
		label:SetSize(140, 40)
		
		CL_GUI.selection.sub_panels[pnal] = true
	end
end

function CL_GUI.SubmitSelection()
	LocalPlayer():ConCommand("_dd_clear_weapon_selection")
	for wep_class,yes in pairs(WEAPONS.client_selection) do
		if yes then LocalPlayer():ConCommand("_dd_select_weapon " .. wep_class) end
	end
end

function CL_GUI.ClearSelection()
	WEAPONS.ClientClearSelection()
	CL_GUI.UpdateActive()
	LocalPlayer():ConCommand("_dd_clear_weapon_selection")
end

function CL_GUI.WeaponVotingPopup()
	--give john freeman wepons!
	if not GAMEMODE.client_ready then return end
	if not LocalPlayer() then return end
	if not CL_GUI.vote then return end
	
	CL_GUI.vote.frame:MakePopup()
	CL_GUI.vote.frame:SetVisible(true)
end
concommand.Add("dd_cl_weapon_vote_popup", CL_GUI.WeaponVotingPopup)

function CL_GUI.WeaponSelectionPopup()
	if not GAMEMODE.client_ready then return end
	if not LocalPlayer() then return end
	if not CL_GUI.selection then return end
	if not CL_GUI.vote then return end
	
	CL_GUI.vote.frame:SetVisible(false)
	
	CL_GUI.selection.frame:MakePopup()
	CL_GUI.selection.frame:SetVisible(true)
end
concommand.Add("dd_cl_weapon_selection_popup", CL_GUI.WeaponSelectionPopup)

function CL_GUI.PlayerSpawnForBattle()
	if not GAMEMODE.client_ready then return end
	if not LocalPlayer() then return end
	if not CL_GUI.selection then return end
	if not CL_GUI.vote then return end
	
	CL_GUI.selection.frame:SetVisible(false)
	CL_GUI.vote.frame:SetVisible(false)
end

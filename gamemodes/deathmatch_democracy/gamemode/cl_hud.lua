
local math = math

local margin = 10

local bg_colors = {
	background_main = Color(0, 0, 10, 200),
	
	noround = Color(100, 100, 100, 200),
	inround = Color(25, 200, 25, 200)
};

local health_colors = {
   border = COLOR_WHITE,
   background = Color(100, 25, 25, 222),
   fill = Color(200, 50, 50, 250)
};

local function ShadowedText(text, font, x, y, color, xalign, yalign)
	--draw.SimpleText(text, font, x+2, y+2, COLOR_BLACK, xalign, yalign)
	draw.SimpleText(text, font, x, y, color, xalign, yalign)
end

local function PaintBar(x, y, w, h, colors, value)
	--background
	draw.RoundedBox(8, x-1, y-1, w+2, h+2, colors.background)
	
	--fill
	local width = w * math.Clamp(value, 0, 1)
	
	if width > 0 then
		draw.RoundedBox(8, x, y, width, h, colors.fill)
	end
end

local function getRoundText()
	if GAMEMODE.round_state == ROUND_WAITING then
		return "Waiting for players."
	elseif GAMEMODE.round_state == ROUND_WEP_VOTING then
		return "Voting for weapons."
	elseif GAMEMODE.round_state == ROUND_ACTIVE then
		return "Go!"
	elseif GAMEMODE.round_state == ROUND_POST then
		return "Waiting for next round."
	else
		return "Error!"
	end
end

local function PlayerHUDPaint(client)
	local x = margin
	local height = 90
	local width = 250
	local y = ScrH() - margin - height
	
	local bar_height = 25
	local bar_width = width - (margin * 2)
	
	--draw health
	local health = math.max(0, client:Health())
	local health_y = y + margin
	PaintBar(x + margin, health_y, bar_width, bar_height, health_colors, health/100)
	ShadowedText(tostring(health), "DermaLarge", bar_width, health_y, COLOR_WHITE, TEXT_ALIGN_RIGHT, TEXT_ALIGN_RIGHT)
	
	--draw round time
	local endtime = GetGlobalFloat("dd_round_end", 0) - CurTime()
	local text = util.SimpleTime(math.max(0, GetGlobalFloat("dd_round_end", 0) - CurTime()), "%02i:%02i")
	
	local round_x = x + margin
	local round_y = y - 30 + 3
	
	--PaintBar(round_x, round_y, bar_width, bar_height, health_colors, 0)
	ShadowedText(text, "DermaLarge", round_x, round_y, COLOR_WHITE)
	
	--draw round status
	local status_y = round_y - 40
	ShadowedText(getRoundText(), "DermaLarge", round_x, status_y, COLOR_WHITE)
end

hook.Add("HUDPaint", "HUDPaint_DM", function()
	local client = LocalPlayer()
	
	if true then
		PlayerHUDPaint(client)
	end
end )


--local hud_standard = {"CHudHealth", "CHudBattery", "CHudAmmo", "CHudSecondaryAmmo"}
local hud_standard = {"CHudHealth"}
function GM:HUDShouldDraw(name)
	for k, v in pairs(hud_standard) do
		--ttt returns false and this is how it disables the standard hud
		if name == v then return false end
	end
	
	return true
end
